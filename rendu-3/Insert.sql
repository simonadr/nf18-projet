INSERT INTO utilisateur VALUES ( 1 );
INSERT INTO utilisateur VALUES ( 2 );
INSERT INTO utilisateur VALUES ( 3 );
INSERT INTO utilisateur VALUES ( 4 );
INSERT INTO utilisateur VALUES ( 5 );
INSERT INTO utilisateur VALUES ( 6 );
INSERT INTO utilisateur VALUES ( 7 );
INSERT INTO utilisateur VALUES ( 8 );
INSERT INTO utilisateur VALUES ( 9 );
INSERT INTO utilisateur VALUES ( 10 );


INSERT INTO vehicule VALUES ('FH-338-RC', 'Modus', 'Renault', 'vehicule simple', 1);
INSERT INTO vehicule VALUES ('FH-440-RC', 'Modus', 'Renault', 'vehicule simple', 2);
INSERT INTO vehicule VALUES ('667-LDO-345', 'Alpine', 'Renault', 'vehicule simple', 3);
INSERT INTO vehicule VALUES ('28-XXX-TT', '458-Italia','ferrari', 'vehicule simple', 4);
INSERT INTO vehicule VALUES ('ZD-111-RC', 'Chiron', 'Bugatti', 'vehicule simple', 5);
INSERT INTO vehicule VALUES ('AAA-001-AAA', 'Dugati', '1299 Panigale R Édition Finale', '2 roues',6);
INSERT INTO vehicule VALUES ('ER-338-RC', 'Polo', 'Volkswagen', 'vehicule simple', 7);
INSERT INTO vehicule VALUES ('FH-440-RB', 'Clio 3 RS', 'Renault', 'vehicule simple', 8);
INSERT INTO vehicule VALUES ('245-PTR-123', 'Model S', 'Tesla', 'vehicule simple',9);
INSERT INTO vehicule VALUES ('123-ASW-549', 'train', 'tcoutchou', 'camion',10);


INSERT INTO zone VALUES ('Portuaire', 15);
INSERT INTO zone VALUES ('Parc', 5);
INSERT INTO zone VALUES ('Ville', 10);
INSERT INTO zone VALUES ('ZII', 8);
INSERT INTO zone VALUES ('ZAC', 8);
INSERT INTO zone VALUES ('ZUC', 8);
INSERT INTO zone VALUES ('ZEP', 8);
INSERT INTO zone VALUES ('Paris 16', 100);
INSERT INTO zone VALUES ('Campagne', 1);
INSERT INTO zone VALUES ('Aeroport', 15);

INSERT INTO parking VALUES ('Bouvines', 'Ville',83,12,92,120,78,130);
INSERT INTO parking VALUES ('Songeon', 'Parc',89,43,32,11,51,70);
INSERT INTO parking VALUES ('Marche aux Herbes', 'Ville',111,22,3,44,11,42);
INSERT INTO parking VALUES ('Bayser', 'Parc',12,45,958,43,21,450);
INSERT INTO parking VALUES ('Chateau', 'Parc',1, 1, 1, 1,1,1);
INSERT INTO parking VALUES ('Gare', 'Ville',43, 23, 53, 432,35,114);
INSERT INTO parking VALUES ('La Ruche', 'ZAC',321, 321, 312, 231,211,251);
INSERT INTO parking VALUES ('Port', 'ZUC',14,432,21,234,15,331);
INSERT INTO parking VALUES ('BF', 'ZII',312, 321, 432, 2,5,421);
INSERT INTO parking VALUES ('PG', 'ZEP',1, 2, 3, 4,5,6);

INSERT INTO compte VALUES ('louislech2001@gmail.com','Louis2001', False);
INSERT INTO compte VALUES ('pierre.guerin0@gmail.com','PIERRE00', True);
INSERT INTO compte VALUES ('adrisim2001@gmail.com','ADRI2001', True);
INSERT INTO compte VALUES ('samuel@gmail.com','Samuel64', False);
INSERT INTO compte VALUES ('Jacquie@gmail.com','jacquie666', True);
INSERT INTO compte VALUES ('OUIOUI@hotmail.com','ouiiii', True);
INSERT INTO compte VALUES ('rubynikara@wanadoo.fr','ruby98', False);
INSERT INTO compte VALUES ('lionel@beauxis.com','GRANDISS', False);
INSERT INTO compte VALUES ('pierrot@gmail.com','1234', True);
INSERT INTO compte VALUES ('antoine@dupont.com','Ministre', False);

INSERT INTO occasionnel VALUES (1,'louislech2001@gmail.com');
INSERT INTO occasionnel VALUES (2,'samuel@gmail.com');
INSERT INTO occasionnel VALUES (3,'lionel@beauxis.com');
INSERT INTO occasionnel VALUES (4,'rubynikara@wanadoo.fr');
INSERT INTO occasionnel VALUES (5,'antoine@dupont.com');

INSERT INTO ticket VALUES (1, '2021-05-08 15:30', 'guichet',1, 'Bouvines');
INSERT INTO ticket VALUES (2, '2021-05-09 11:30', 'guichet', 2,'Bouvines');
INSERT INTO ticket VALUES (3, '2021-05-09 12:35', 'guichet', 3,'Bouvines');
INSERT INTO ticket VALUES (4, '2021-05-10 12:17', 'automate',4, 'Songeon');
INSERT INTO ticket VALUES (5, '2021-05-13 12:30', 'automate', 5,'Songeon');
INSERT INTO ticket VALUES (6, '2021-05-14 08:35', 'automate', 1,'BF');
INSERT INTO ticket VALUES (7, '2021-05-14 10:15', 'automate', 2,'PG');
INSERT INTO ticket VALUES (8, '2021-05-14 18:30', 'automate', 3,'Port');

INSERT INTO abonne VALUES (1, 'Guerin', 'Pierre',1, 'pierre.guerin0@gmail.com');
INSERT INTO abonne VALUES (2, 'Simon', 'Adrien',2, 'adrisim2001@gmail.com');
INSERT INTO abonne VALUES (3, 'Jacques', 'Jacquie',10, 'Jacquie@gmail.com');
INSERT INTO abonne VALUES (4, 'Oui', 'Oui',8, 'OUIOUI@hotmail.com');
INSERT INTO abonne VALUES (5, 'pierrot', 'roppie',2, 'pierrot@gmail.com');

INSERT INTO abonnement VALUES (1,150,30,'01-06-2022','Bouvines',2);
INSERT INTO abonnement VALUES (2, 100, 60, '02-07-2022','Songeon',3);
INSERT INTO abonnement VALUES (3,50,90,'01-08-2022','Bouvines',4);
INSERT INTO abonnement VALUES (4, 300, 1000, '02-09-2022','BF',5);
INSERT INTO abonnement VALUES (5,120,10,'01-10-2022','PG',1);
INSERT INTO abonnement VALUES (6, 90, 80, '02-11-2022','Port',1);
INSERT INTO abonnement VALUES (7,50,30,'01-12-2022','Songeon',3);



INSERT INTO place VALUES (1,'camion','Bouvines');
INSERT INTO place VALUES (2,'vehicule simple', 'Bouvines');
INSERT INTO place VALUES (3,'vehicule simple','BF');
INSERT INTO place VALUES (4,'2 roues','BF');
INSERT INTO place VALUES (5,'2 roues', 'PG');
INSERT INTO place VALUES (6,'vehicule simple','Gare');
INSERT INTO place VALUES (7,'camion','Gare');
INSERT INTO place VALUES (8,'vehicule simple', 'Bayser');
INSERT INTO place VALUES (9,'vehicule simple','Chateau');
INSERT INTO place VALUES (10,'2 roues','La Ruche');

INSERT INTO reservation VALUES (1, '2021-05-09 11:30', 'abonne', 'pierre.guerin0@gmail.com',2,'Bouvines');
INSERT INTO reservation VALUES (2, '2022-08-16 18:30', 'abonne', 'adrisim2001@gmail.com',3,'BF');
INSERT INTO reservation VALUES (3, '2022-08-17 18:30', 'automate', 'lionel@beauxis.com',7,'Gare');
INSERT INTO reservation VALUES (4, '2022-09-02 12:00', 'guichet', 'antoine@dupont.com',4,'BF');
