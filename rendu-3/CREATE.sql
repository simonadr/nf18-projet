DROP VIEW IF EXISTS comptes_doubles;
DROP TABLE IF EXISTS reservation;
DROP TABLE IF EXISTS place;
DROP TABLE IF EXISTS abonnement;
DROP TABLE IF EXISTS abonne;
DROP TABLE IF EXISTS ticket;
DROP TABLE IF EXISTS occasionnel;
DROP TABLE IF EXISTS compte;
DROP TABLE IF EXISTS parking;
DROP TABLE IF EXISTS zone;
DROP TABLE IF EXISTS vehicule;
DROP TABLE IF EXISTS utilisateur;
DROP TYPE IF EXISTS typeVehicule;
DROP TYPE IF EXISTS typePaiement;



CREATE TYPE typePaiement AS ENUM ('guichet','automate','abonne');
CREATE TYPE typeVehicule AS ENUM ('camion','2 roues','vehicule simple');

CREATE TABLE utilisateur (
id INTEGER PRIMARY KEY
);

CREATE TABLE vehicule (
immat VARCHAR PRIMARY KEY,
modele VARCHAR NOT NULL,
marque VARCHAR NOT NULL,
type typeVehicule NOT NULL,
proprietaire INTEGER NOT NULL,
FOREIGN KEY (proprietaire) REFERENCES utilisateur(id)
);

CREATE TABLE zone (
nom VARCHAR PRIMARY KEY,
prix FLOAT CHECK (prix>0)
);

CREATE TABLE parking (
nom VARCHAR PRIMARY KEY,
zone VARCHAR REFERENCES zone(nom),
NbreDeuxRouesCouvertes INTEGER NOT NULL,
NbreDeuxRouesAir INTEGER NOT NULL,
NbreCamionCouvertes INTEGER NOT NULL,
NbreCamionAir INTEGER NOT NULL,
NbreSimpleCouvertes INTEGER NOT NULL,
NbreSimpleAir INTEGER NOT NULL
);


CREATE TABLE compte (
mail VARCHAR PRIMARY KEY,
mdp VARCHAR NOT NULL,
abonne BOOLEAN NOT NULL
);

CREATE TABLE occasionnel (
id INTEGER,
compte VARCHAR,
PRIMARY KEY (id),
FOREIGN KEY (id) REFERENCES utilisateur(id),
FOREIGN KEY (compte) REFERENCES compte(mail),
UNIQUE (compte)
);

CREATE TABLE ticket (
id INTEGER PRIMARY KEY,
date TIMESTAMP NOT NULL,
type_transac typePaiement NOT NULL,
occasionnel INTEGER,
parking VARCHAR REFERENCES parking(nom) NOT NULL,
FOREIGN KEY (occasionnel) REFERENCES occasionnel(id)
);

CREATE TABLE abonne (
id INTEGER,
nom VARCHAR NOT NULL,
prenom VARCHAR NOT NULL,
compteurFidelite INTEGER CHECK(compteurFidelite > 0),
compte VARCHAR REFERENCES compte(mail) NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (id) REFERENCES utilisateur(id)
);

CREATE TABLE abonnement (
num_abo INTEGER PRIMARY KEY,
prixAbonnement FLOAT NOT NULL,
duree INTEGER CHECK (duree>0) NOT NULL,
date_fin DATE NOT NULL,
parking  VARCHAR REFERENCES parking(nom),
abonne INTEGER REFERENCES abonne(id)
);

CREATE TABLE place (
id INTEGER,
vehicule typeVehicule NOT NULL,
parking VARCHAR,
PRIMARY KEY (id, parking)
);

CREATE TABLE reservation (
id INTEGER,
date TIMESTAMP NOT NULL,
type_transac typePaiement NOT NULL,
compte VARCHAR REFERENCES compte(mail),
place_id INTEGER,
place_parking VARCHAR,
FOREIGN KEY (place_id,place_parking) REFERENCES place(id, parking),
PRIMARY KEY (id, place_id, place_parking)
);

CREATE VIEW comptes_doubles AS
SELECT compte FROM occasionnel
INTERSECT
SELECT compte FROM abonne;
