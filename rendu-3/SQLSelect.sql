Somme des prix des tickets achetés par l’utilisateur 1 :
SELECT SUM(prix) FROM Zone 
INNER JOIN parking ON (parking.zone=zone.nom)
INNER JOIN ticket ON (parking.nom=ticket.parking)
INNER JOIN occasionnel ON (ticket.occasionnel=occasionnel.id And occasionnel.id=1)

Suppression de compte ayant pour mail ‘adrisim2001@gmail.com’ :

x = SELECT id FROM abonne WHERE(compte='adrisim2001@gmail.com');#(marche en python)
DELETE FROM reservation WHERE (compte = 'adrisim2001@gmail.com');
DELETE FROM abonnement WHERE(abonne = x);
DELETE FROM abonne WHERE(compte='adrisim2001@gmail.com');
DELETE FROM compte WHERE(mail= 'adrisim2001@gmail.com');



Doubler le prix de la zone ‘Portuaire’ :
UPDATE zone
SET prix = prix*2
WHERE nom =‘Portuaire’;

Trouver le nombre de places disponibles dans tous les parkings :
SELECT SUM(NbreDeuxRouesCouvertes + NbreDeuxRouesAir + NbreCamionCouvertes + NbreCamionAir + NbreSimpleCouvertes + NbreSimpleAir)
FROM parking;

Trouver le nombre de places disponibles dans le parking ‘Bouvines’ :
SELECT SUM(NbreDeuxRouesCouvertes + NbreDeuxRouesAir + NbreCamionCouvertes + NbreCamionAir + NbreSimpleCouvertes + NbreSimpleAir)
FROM parking WHERE parking.nom = 'Bouvines'
