#Parking ou le nombre de places pour les 2 roues est supérieur à 100
SELECT * FROM parking
WHERE ((nbreplaces->>'NbreDeuxRouesCouvertes')::integer + (NbrePlaces->>'NbreDeuxRouesAir')::integer >100);

#Nombre total de places dans le parking Bouvines
SELECT (
(nbreplaces->>'NbreDeuxRouesCouvertes')::INTEGER +
(nbreplaces->>'NbreDeuxRouesAir')::INTEGER +
(nbreplaces->>'NbreCamionCouvertes')::INTEGER +
(nbreplaces->>'NbreCamionAir')::INTEGER +
(nbreplaces->>'NbreSimpleCouvertes')::INTEGER +
(nbreplaces->>'NbreSimpleAir')::INTEGER) AS NombreDePlaces
FROM parking WHERE parking.nom='Bouvines';

#Vehicule dont le modèle est une Polo
SELECT *
FROM vehicule
WHERE infos->>'modele' = 'Polo';

#Prix de l abonnement pour l abonné dont le numéro est 1
SELECT infosAbo->>'prix' as prix FROM abonnement WHERE num_abo = 1;

#Prix par jour des abonnements
SELECT ((infosAbo->>'prix')::float / (infosAbo->>'duree')::float)
AS PrixParJour
FROM abonnement;
