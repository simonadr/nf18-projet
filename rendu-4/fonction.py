import psycopg2


def listeAbonne(conn):
    cur = conn.cursor()
    sql = "SELECT * FROM abonne"
    cur.execute(sql)
    raw = cur.fetchone()
    while raw:
        print(raw[1], end=" ")
        print(raw[2])
        raw = cur.fetchone()

    print(" ")
    print(" ")


def ajouterAbonne(conn, facteurPrix):
    cur = conn.cursor()
    sql = "SELECT MAX(id) FROM utilisateur"
    cur.execute(sql)
    raw = cur.fetchone()
    idUtilisateur = int(raw[0])
    idUtilisateur += 1

    # ajout dans tables utilisateur
    sql = f"INSERT INTO utilisateur VALUES ( {idUtilisateur} )"
    cur.execute(sql)
    conn.commit()

    sql = "SELECT MAX(num_abo) FROM abonnement"
    cur.execute(sql)
    raw = cur.fetchone()
    num_abo = int(raw[0])
    num_abo += 1

    nbreVehicule = int(input("Combien de véhicules avez-vous? ,"))
    for i in range(nbreVehicule):
        immat = input("Immatriculation du véhicule: ")
        modele = input("Modèle du véhicule: ")
        marque = input("Marque du véhicule: ")
        verif = 0
        while verif != 1:
            typev = input("Type du véhicule (vehicule simple, 2 roues, camion)")
            if typev == "vehicule simple" or typev == "2 roues" or typev == "camion":
                verif = 1
            else:
                print("Le type du véhicule n'est pas valide")
        sql = f"INSERT INTO vehicule VALUES ('{immat}', '{modele}', '{marque}', '{typev}', {idUtilisateur})"
        cur.execute(sql)
        conn.commit()
    nom = input("Quel est votre nom: ")
    prenom = input("Quel est votre prenom: ")
    mail = input("Quel est votre mail: ")
    mdp = input("Mot de passe de votre compte: ")

    # ajout dans la table compte
    sql = f"INSERT INTO compte VALUES ('{mail}','{mdp}', True)"
    cur.execute(sql)
    conn.commit()

    # ajout dans la table abonne
    sql = (
        f"INSERT INTO abonne VALUES ({idUtilisateur}, '{nom}', '{prenom}',1, '{mail}')"
    )
    cur.execute(sql)
    conn.commit()

    # Ajout dans la table abonnement
    print("Les parkings suivants sont disponible: ")
    sql = "SELECT * FROM parking"
    cur.execute(sql)
    raw = cur.fetchone()
    while raw:
        print(raw[0])
        raw = cur.fetchone()
    print(" ")
    parking = input("Dans quel parking voulez-vous prendre un abonnement ?")
    try:
        sql = f"SELECT zone.prix FROM zone INNER JOIN parking ON parking.zone=zone.nom WHERE parking.nom = '{parking}'"
        cur.execute(sql)
        raw = cur.fetchone()
        prix = float(raw[0])
        prix = prix * facteurPrix
    except Exception as e:
        print(e)

    try:
        sql = f"INSERT INTO abonnement VALUES ({num_abo},{prix},30,date_format(CURDATE()+30,'%d/%m/%Y'),'{parking}',{num_abo})"
        cur.execute(sql)
        conn.commit()
    except Exception as e:
        print(e)


def ajouterOccasionnel(conn):

    cur = conn.cursor()
    sql = "SELECT MAX(id) FROM utilisateur"
    cur.execute(sql)
    raw = cur.fetchone()
    idUtilisateur = int(raw[0])
    idUtilisateur += 1

    # ajout dans tables utilisateur
    sql = f"INSERT INTO utilisateur VALUES ( {idUtilisateur} )"
    cur.execute(sql)
    conn.commit()

    nbreVehicule = int(input("Combien de véhicules avez-vous? ,"))
    for i in range(nbreVehicule):
        immat = input("Immatriculation du véhicule: ")
        modele = input("Modèle du véhicule: ")
        marque = input("Marque du véhicule: ")
        verif = 0
        while verif != 1:
            typev = input("Type du véhicule (vehicule simple, 2 roues, camion)")
            if typev == "vehicule simple" or typev == "2 roues" or typev == "camion":
                verif = 1
            else:
                print("Le type du véhicule n'est pas valide")
        sql = f"INSERT INTO vehicule VALUES ('{immat}', '{modele}', '{marque}', '{typev}', {idUtilisateur})"
        cur.execute(sql)
        conn.commit()

    tmp = 0
    while tmp == 0:
        reponse = input(
            "Voulez vous créer un compte qui vous permet de faire une réservation en ligne? (Oui/Non) : "
        )
        if reponse == "Oui" or reponse == "Non":
            tmp = 1
        else:
            print("Veuilez saisir Oui/Non ")

    if reponse == "Oui":
        mail = input("Quel est votre mail: ")
        mdp = input("Mot de passe de votre compte: ")

        # ajout dans la table compte
        sql = f"INSERT INTO compte VALUES ('{mail}','{mdp}', False)"
        cur.execute(sql)
        conn.commit()

        # ajout dans la table occasionnel
        sql = f"INSERT INTO occasionnel VALUES ({idUtilisateur},'{mail}')"
        cur.execute(sql)
        conn.commit()
    else:
        # ajout dans la table occasionnel
        sql = f"INSERT INTO occasionnel VALUES ({idUtilisateur},NULL)"
        cur.execute(sql)
        conn.commit()


def nbPlacesDansParking(conn):
    # affiche les parkings disponibles
    cur = conn.cursor()
    print("Vous pouvez consulter  les places disponibles pour les parkings suivants: ")
    sql = "SELECT * FROM parking"
    cur.execute(sql)
    raw = cur.fetchone()
    while raw:
        print(raw[0])
        raw = cur.fetchone()
    print(" ")
    nomParking = input("Parmis ces parkings, lequel vous intéresse?: ")
    sql = f"SELECT * FROM parking WHERE parking.nom = '{nomParking}'"
    cur.execute(sql)
    raw = cur.fetchone()
    if not raw:
        print("Le parking n'éxiste pas")
        return

    try:
        sql = f"SELECT SUM(NbreDeuxRouesCouvertes + NbreDeuxRouesAir + NbreCamionCouvertes + NbreCamionAir + NbreSimpleCouvertes + NbreSimpleAir) FROM parking WHERE parking.nom = '{nomParking}'"
        cur.execute(sql)
        nb = cur.fetchone()[0]
        print(f"Il y a {nb} places libres dans le parking {nomParking}")
    except Exception as e:
        print("Erreur; veuillez verifier votre saisie")
        print(f"Message systeme: {e}")
        print("Retour au menu")


def supprimerOccasionnel(conn):
    cur = conn.cursor()
    # recherche vehicule pour connaitre l'id de l'utilisateur
    tmp = 0
    while tmp == 0:
        try:
            im = input("veuillez rentrer l'immatriculation d'un de vos véhicule")
            sql = f"SELECT * FROM vehicule WHERE immat = '{im}'"
            cur.execute(sql)
            raw = cur.fetchone()
        except Exception as e:
            print("Erreur; veuillez verifier votre saisie")
            print(f"Message systeme: {e}")
            print("Retour au menu")
            return

        if raw:
            idUtilisateur = raw[4]
            tmp = 1
        else:
            print("Ce véhicule n'éxiste pas")

    # suppresion de ses véhicules:
    sql = f"DELETE FROM vehicule WHERE(proprietaire='{idUtilisateur}')"
    cur.execute(sql)
    conn.commit()

    # supprime réservation si nécessaire
    reponse = int(input("Avez-vous déjà réalisé une reservation? (1:oui , 2:non) "))
    if reponse == 1:
        mail = input("veuillez saisir votre adresse mail")
        try:
            sql = f"SELECT * FROM compte WHERE(mail='{mail}')"
            cur.execute(sql)
        except Exception as e:
            print("Erreur; veuillez verifier votre saisie")
            print(f"Message systeme: {e}")
            print("Retour au menu")
            return

        sql = f"DELETE FROM occasionnel WHERE(compte='{mail}')"
        cur.execute(sql)
        conn.commit()
        sql = f"DELETE FROM compte WHERE(mail='{mail}')"
        cur.execute(sql)
        conn.commit()
    else:
        sql = f"DELETE FROM occasionnel WHERE(id='{idUtilisateur}')"
        cur.execute(sql)
        conn.commit()


def supprimerAbonne(conn, mail):
    try:
        cur = conn.cursor()
        sql = f"SELECT id FROM abonne WHERE(compte='{mail}')"
        cur.execute(sql)
        ident = cur.fetchone()[0]
    except Exception as e:
        print("Erreur; veuillez verifier votre saisie")
        print(f"Message systeme: {e}")
        print("Retour au menu")
        return
    try:
        sql = f"SELECT * FROM compte WHERE(mail='{mail}')"
        cur.execute(sql)
    except Exception as e:
        print("Erreur; veuillez verifier votre saisie")
        print(f"Message systeme: {e}")
        print("Retour au menu")
        return
    try:
        sql = f"SELECT * FROM abonnement WHERE(abonne='{ident}')"
        cur.execute(sql)
    except Exception as e:
        print("Erreur; veuillez verifier votre saisie")
        print(f"Message systeme: {e}")
        print("Retour au menu")
        return
    try:
        sql = f"SELECT * FROM reservation WHERE(compte='{mail}')"
        cur.execute(sql)
        sql = f"DELETE FROM reservation WHERE (compte='{mail}')"
        cur.execute(sql)
    except Exception as e:
        print("Erreur; veuillez verifier votre saisie")
        print(f"Message systeme: {e}")
        print("Retour au menu")
        return
    sql = f"DELETE FROM abonnement WHERE(abonne='{ident}')"
    cur.execute(sql)
    conn.commit()
    sql = f"DELETE FROM abonne WHERE(compte='{mail}')"
    cur.execute(sql)
    conn.commit()
    sql = f"DELETE FROM compte WHERE(mail='{mail}')"
    cur.execute(sql)
    conn.commit()


def ajouterZone(conn, nomZone, prix):
    try:
        cur = conn.cursor()
        sql = f"INSERT INTO zone VALUES ('{nomZone}', {prix})"
        cur.execute(sql)
        conn.commit()
    except Exception as e:
        print("Erreur; veuillez verifier votre saisie")
        print(f"Message systeme: {e}")
        print("Retour au menu")


def afficherZones(conn):
    cur = conn.cursor()
    sql = "SELECT * FROM zone"
    cur.execute(sql)
    raw = cur.fetchone()
    while raw:
        print("Nom: ", raw[0], end=" ")
        print("prix: ", raw[1], "€")
        raw = cur.fetchone()
    print(" ")


def ajouterParking(conn):
    park = input("Quel est le nom du parking que vous voulez ajouter: ")
    sql = f"SELECT * FROM parking WHERE(nom='{park}')"
    cur = conn.cursor()
    cur.execute(sql)
    raw = cur.fetchone()

    if raw:
        print("Le parking éxiste déjà")
    else:
        afficherZones(conn)
        zone = input("Dans quelle zone voulez vous mettre votre parking: ")

        sql = f"SELECT * FROM zone WHERE(nom='{zone}')"
        cur = conn.cursor()
        cur.execute(sql)
        raw = cur.fetchone()

        if not raw:
            print("La zone n'éxiste pas")
            return

        tmp = 0
        while tmp == 0:
            NbreDeuxRouesCouvertes = int(
                input("Nombre de place pour deux roues couvertes: ")
            )
            if NbreDeuxRouesCouvertes > 0:
                tmp = 1
            else:
                print("La valeur saisie doit être strictement positive")

        tmp = 0
        while tmp == 0:
            NbreDeuxRouesAir = int(
                input("Nombre de place pour deux roues non couvertes: ")
            )
            if NbreDeuxRouesAir > 0:
                tmp = 1
            else:
                print("La valeur saisie doit être strictement positive")

        tmp = 0
        while tmp == 0:
            NbreCamionCouvertes = int(input("Nombre de place pour camion couvertes: "))
            if NbreCamionCouvertes > 0:
                tmp = 1
            else:
                print("La valeur saisie doit être strictement positive")

        tmp = 0
        while tmp == 0:
            NbreCamionAir = int(input("Nombre de place pour camion non couvertes: "))
            if NbreCamionAir > 0:
                tmp = 1
            else:
                print("La valeur saisie doit être strictement positive")

        tmp = 0
        while tmp == 0:
            NbreSimpleCouvertes = int(
                input("Nombre de place pour véhiucle simple couvertes: ")
            )
            if NbreSimpleCouvertes > 0:
                tmp = 1
            else:
                print("La valeur saisie doit être strictement positive")

        tmp = 0
        while tmp == 0:
            NbreSimpleAir = int(
                input("Nombre de place pour vehicule simple non couvertes: ")
            )
            if NbreSimpleAir > 0:
                tmp = 1
            else:
                print("La valeur saisie doit être strictement positive")

        sql = f"INSERT INTO parking VALUES ('{park}', '{zone}', {NbreDeuxRouesCouvertes}, {NbreDeuxRouesAir},{NbreCamionCouvertes},{NbreCamionAir},{NbreSimpleCouvertes},{NbreSimpleAir})"
        cur.execute(sql)
        conn.commit()


def supprimerBDD(conn):
    try:
        cur = conn.cursor()
        sql = "DROP VIEW comptes_doubles;\nDROP TABLE reservation;\nDROP TABLE place;\nDROP TABLE abonnement;\nDROP TABLE abonne;\nDROP TABLE ticket;\nDROP TABLE occasionnel;\nDROP TABLE compte;\nDROP TABLE parking;\nDROP TABLE zone;\nDROP TABLE vehicule;\nDROP TABLE utilisateur;\n\nDROP TYPE typeVehicule;\nDROP TYPE typePaiement;"
        cur.execute(sql)
        conn.commit()
        print("Base de donees supprimee \n\n")
    except Exception as e:
        print(f"Message systeme: {e}")
        print("Retour au menu")


def modifierTarifZone(conn, nomZone, nouveauTarif):
    try:
        cur = conn.cursor()
        sql = f"UPDATE zone SET prix ={nouveauTarif} WHERE nom = '{nomZone}'"
        cur = conn.cursor()
        cur.execute(sql)
        conn.commit()
    except Exception as e:
        print("Erreur; veuillez verifier votre saisie")
        print(f"Message systeme: {e}")
        print("Retour au menu")


def modifierZoneParking(conn, nomParking, nomZone):
    try:
        cur = conn.cursor()
        sql = f"UPDATE parking SET zone ='{nomZone}' WHERE nom = '{nomParking}'"
        cur = conn.cursor()
        cur.execute(sql)
        conn.commit()
    except Exception as e:
        print("Erreur; veuillez verifier votre saisie")
        print(f"Message systeme: {e}")
        print("Retour au menu")


def reservationAbonne(
    conn, date, typeFournisseurPlace, compte, typeVehicule, nomParking
):
    try:
        cur = conn.cursor()
        sql = f"INSERT INTO reservation ('TO_DATE({date}, 'YYYY-MM-DD')','{typeFournisseurPlace}','{compte}','{typeVehicule}','{nomParking}')"
        cur = conn.cursor()
        cur.execute(sql)
    except Exception as e:
        print("Erreur; veuillez verifier votre saisie")
        print(f"Message systeme: {e}")
        print("Retour au menu")


def nombrePlacesPrises(conn):
    tmp = 0
    while tmp == 0:
        num = int(
            input(
                "Sur quel type de tickets voulez-vous avoir une statistique? (1:abonne, 2:guichet, 3:automate"
            )
        )
        if num == 1 or num == 2 or num == 3:
            tmp = 1
        else:
            print("Veuillez renseigner un chiffre entre 1 et 3")

    if num == 1:
        typeTransaction = "abonne"

    if num == 2:
        typeTransaction = "guichet"

    if num == 3:
        typeTransaction = "automate"

    cur = conn.cursor()

    # compte nombre de billets du type voulu
    sql = f"SELECT COUNT(*) FROM ticket WHERE type_transac = '{typeTransaction}'"
    cur.execute(sql)
    raw = cur.fetchone()
    print("Nombre de tickets pris par un ", typeTransaction, " est ", raw[0])


def NbreTicketsParkings(conn):
    tmp = 0
    while tmp == 0:
        num = int(
            input(
                "Sur quel type de tickets voulez-vous avoir une statistique? (1:abonne, 2:guichet, 3:automate"
            )
        )
        if num == 1 or num == 2 or num == 3:
            tmp = 1
        else:
            print("Veuillez renseigner un chiffre entre 1 et 3")

    if num == 1:
        typeTransaction = "abonne"

    if num == 2:
        typeTransaction = "guichet"

    if num == 3:
        typeTransaction = "automate"

    cur = conn.cursor()

    # compte nombre de billets du type voulu
    sql = f"SELECT COUNT(*) FROM ticket WHERE type_transac = '{typeTransaction}'"
    cur.execute(sql)
    raw = cur.fetchone()
    print("Nombre de tickets pris par un ", typeTransaction, " est ", raw[0])
    nombre = int(raw[0])

    # compte nombre de places totales
    sql = "SELECT COUNT(*) FROM ticket"
    cur.execute(sql)
    raw = cur.fetchone()
    nombreTotal = int(raw[0])

    # calcul le pourcentage
    pourcentage = nombre / nombreTotal * 100
    print("Cela représente ", pourcentage, " % des places totales")


def NbreTicketsParkingsParticulier(conn):
    tmp = 0
    while tmp == 0:
        num = int(
            input(
                "Sur quel type de tickets voulez-vous avoir une statistique? (1:abonne, 2:guichet, 3:automate)"
            )
        )
        if num == 1 or num == 2 or num == 3:
            tmp = 1
        else:
            print("Veuillez renseigner un chiffre entre 1 et 3")

    if num == 1:
        typeTransaction = "abonne"

    if num == 2:
        typeTransaction = "guichet"

    if num == 3:
        typeTransaction = "automate"

    cur = conn.cursor()

    # affichage des parkings disponibles
    sql = "SELECT * FROM parking"
    cur.execute(sql)
    print("Parking disponibles: ")
    raw = cur.fetchone()
    while raw:
        print(raw[0])
        raw = cur.fetchone()
    print(" ")
    # choix du parking voulu
    tmp = 0
    while tmp == 0:
        park = input("Sur quel type parking voulez-vous avoir une statistique? ")
        sql = f"SELECT * FROM parking WHERE nom = '{park}'"
        cur.execute(sql)
        raw = cur.fetchone()
        if raw:
            tmp = 1
        else:
            print("Ce parking n'éxiste pas")

    # compte nombre de billets du type voulu dans le parking
    sql = f"SELECT COUNT(*) FROM ticket WHERE type_transac = '{typeTransaction}' AND parking = '{park}'"
    cur.execute(sql)
    raw = cur.fetchone()
    print("Nombre de tickets pris par un ", typeTransaction, " est ", raw[0])
    nombre = int(raw[0])

    # compte nombre de places totales dans ce parking
    sql = f"SELECT COUNT(*) FROM ticket WHERE parking = '{park}'"
    cur.execute(sql)
    raw = cur.fetchone()
    nombreTotal = int(raw[0])

    # calcul le pourcentage
    pourcentage = nombre / nombreTotal * 100
    print("Cela représente ", pourcentage, " % des places totales")


def reservationOccasionnel(conn, date, typeFournisseurPlace, typeVehicule, nomParking):
    try:
        cur = conn.cursor()
        sql = f"INSERT INTO reservation ('TO_DATE({date}, 'YYYY-MM-DD')','{typeFournisseurPlace}','{typeVehicule}','{nomParking}')"
        cur = conn.cursor()
        cur.execute(sql)
    except Exception as e:
        print("Erreur; veuillez verifier votre saisie")
        print(f"Message systeme: {e}")
        print("Retour au menu")


def ajouterVehicule(conn):
    proprio = input("Entrez l'adresse mail du propriétaire: ")
    cur = conn.cursor()
    sql = f"SELECT id FROM occasionnel WHERE compte = '{proprio}'"
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        proprio_id = raw[0]
    else:
        sql = f"SELECT id FROM abonne WHERE compte = '{proprio}'"
        cur.execute(sql)
        raw = cur.fetchone()
        if raw:
            proprio_id = raw[0]
        else:
            print("Ce propriétaire n'existe pas")
            return

    immat = input("Entrez l'immatriculation du vehicule: ")
    modele = input("Entrez le modele du vehicule: ")
    marque = input("Entrez la marque du vehicule: ")
    type = 0
    while type not in [1, 2, 3]:
        type = int(
            input(
                "Entrez le type du vehicule: (1:véhicule simple, 2:deux roues, 3:camion)"
            )
        )
        if type == 1:
            type = "véhicule simple"
        elif type == 2:
            type = "2 roues"
        elif type == 3:
            type = "camion"
    cur = conn.cursor()
    sql = f"INSERT INTO vehicule ('{immat}','{modele}','{marque}','{type}','{proprio_id}')"
    cur.execute(sql)


def majReduction(conn):
    abo = input("Entrez l'adresse mail du propriétaire: ")
    facteur = int(input("Entrez un nouveau facteur de réduction : "))
    cur = conn.cursor()
    sql = f"UPDATE abonne SET compteurFidelite = '{facteur}' WHERE compte = '{abo}'"
    cur.execute(sql)


def nombreOccasionnel(conn):
    cur = conn.cursor()
    try:
        sql = "SELECT COUNT(*) FROM occasionnel"
        cur.execute(sql)
        raw = cur.fetchone()
        nombre = int(raw[0])
        print("Il y a ", nombre, " occasionnels différents")
        print(" ")
    except Exception as e:
        print(f"Message systeme: {e}")
        print("Retour au menu")


def nombreAbonne(conn):
    cur = conn.cursor()
    try:
        sql = "SELECT COUNT(*) FROM abonne"
        cur.execute(sql)
        raw = cur.fetchone()
        nombre = int(raw[0])
        print("Il y a ", nombre, " abonne différents")
        print(" ")
    except Exception as e:
        print(f"Message systeme: {e}")
        print("Retour au menu")


def nombreUtilisateurs(conn):
    cur = conn.cursor()
    try:
        sql = "SELECT COUNT(*) FROM utilisateur"
        cur.execute(sql)
        raw = cur.fetchone()
        nombre = int(raw[0])
        print("Il y a ", nombre, " utilisateurs différents")
        print(" ")
    except Exception as e:
        print(f"Message systeme: {e}")
        print("Retour au menu")


def nombrePlacesLibres(conn):
    cur = conn.cursor()
    # affiche les parkings disponibles
    print("Vous pouvez consulter  les places disponibles pour les parkings suivants: ")
    sql = "SELECT * FROM parking"
    cur.execute(sql)
    raw = cur.fetchone()
    while raw:
        print(raw[0])
        raw = cur.fetchone()
    print(" ")
    park = input("Parmis ces parkings, lequel vous intéresse?: ")

    # Choix du type de vehicule
    typeV = input(
        "Quel type de place vous intéresse: 2 roues / camion / vehicule simple"
    )


def nombreDeuxRouesEnregistres(conn):
    cur = conn.cursor()
    try:
        sql = "SELECT COUNT(*) FROM vehicule WHERE type= '2 roues'"
        cur.execute(sql)
        raw = cur.fetchone()
        print("Nombre de 2 roues : ", int(raw[0]))
    except Exception as e:
        print(f"Message systeme: {e}")
        print("Retour au menu")


def nombreCamionsEnregistres(conn):
    cur = conn.cursor()
    try:
        sql = "SELECT COUNT(*) FROM vehicule WHERE type= 'camion'"
        cur.execute(sql)
        raw = cur.fetchone()
        print("Nombre de camions : ", int(raw[0]))
    except Exception as e:
        print(f"Message systeme: {e}")
        print("Retour au menu")


def nombreVSEnregistres(conn):
    cur = conn.cursor()
    try:
        sql = "SELECT COUNT(*) FROM vehicule WHERE type= 'vehicule simple'"
        cur.execute(sql)
        raw = cur.fetchone()
        print("Nombre de vehicules simples : ", int(raw[0]))
    except Exception as e:
        print(f"Message systeme: {e}")
        print("Retour au menu")
