import psycopg2
import fonction

HOST = "tuxa.sme.utc"
USER = "nf18p013"  # "nf18p008"
PASSWORD = "XLFuujH5"  # "IeBhD9xe"
DATABASE = "dbnf18p013"
# Open connection
conn = psycopg2.connect(
    "host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD)
)
# Open a cursor to send SQL commands
cur = conn.cursor()


# menu

main_user_choice = 0
while main_user_choice != 5:
    print("1/ Gestion des utilisateurs et des réservations")
    print("2/ Gestion des parkings et des zones")
    print("3/ Statistiques générales")
    print("4/ Maintenance BDD")
    print("5/ Quitter")

    main_user_choice = int(input("Dans quel menu souhaitez vous vous rendre ? "))

    if main_user_choice == 1:
        print("11/ Ajouter un abonné")
        print("12/ Ajouter un occasionnel")
        print("13/ Supprimer un abonné")
        print("14/ Renouveler abonnement")
        print("15/ Visualiser la liste des abonnés")
        print("16/ Créer une réservation abonné")
        print("17/ Créer une réservation occasionnel")
        print("18/ Mettre à jour les réductions fidélité")

    if main_user_choice == 2:
        print("21/ Ajouter zone")
        print("22/ Ajouter parking")
        print("23/ Modifier le tarif d'une zone")
        print("24/ Visualiser le nombre de places dispos dans un parking")
        print("25/ Modifier la zone d'un parking ")

    if main_user_choice == 3:
        print("31/ Renvoyer le nombre de places prises par abonne/ticket/automate")
        print("32/ Statistiques des tickets pris sur un parking Particulier")
        print("33/ Statistiques des tickets pris sur l'ensemble des parkings")
        print("34/ Nombre d'abonne")
        print("35/ Nombre d'occasionnel")
        print("36/ Nombre total d'utilisateur différents")
        print("37/ Nombre de 2 roues enregistrés")
        print("38/ Nombre de camions enregistrés")
        print("39/ Nombre de véhicules simples enregistrés")

    if main_user_choice == 4:
        print("41/ insertion automatique de donnees")
        print("42/ Creer/Reinitialiser la BDD")
        print("43/ Supprimer la BDD")

    if main_user_choice in [1, 2, 3, 4]:
        user_choice = int(input("Que souhaitez vous faire ? "))
    else:
        user_choice = 0

    # SOUS MENU 1

    if user_choice == 11:
        qst = int(input("etes vous déja venu ? oui : 1, non : 2"))
        if qst == 1:
            fonction.supprimerOccasionnel(conn)
        fonction.ajouterAbonne(conn, 25)

    elif user_choice == 12:
        fonction.ajouterOccasionnel(conn)

    elif user_choice == 13:
        mail = input("adresse mail de l'abonne à supprimer :")
        fonction.supprimerAbonne(conn, mail)

    elif user_choice == 14:
        idAbo = int(
            input(
                "Quel est le numero d'identifiant de l'abonné dont il faut renouveler l'abonnement ?"
            )
        )
        fonction.renouvelerAbonnement(conn, idAbo)

    elif user_choice == 15:
        fonction.listeAbonne(conn)

    elif user_choice == 16:
        date = input("date de la réservation (YYYY-MM-DD)")
        choix = int(input("Abonne : 1 | Guichet : 2 | Automate : 3 "))
        if choix == 1:
            typeFournisseurPlace = "abonne"
        elif choix == 2:
            typeFournisseurPlace = "guichet"
        elif choix == 3:
            typeFournisseurPlace = "automate"
        compte = input("Quel est l'email du compte ?")
        choix = int(input("Camion : 1 | Deux roues : 2 | Vehicule simple : 3 "))
        if choix == 1:
            typeVehicule = "camion"
        elif choix == 2:
            typeVehicule = "2 roues"
        elif choix == 3:
            typeVehicule = "vehicule simple"
        nomParking = input("Quel est le nom du parking concerne par la réservation?")
        fonction.reservationAbonne(
            conn, date, typeFournisseurPlace, compte, typeVehicule, nomParking
        )

    elif user_choice == 17:
        date = input("date de la réservation (YYYY-MM-DD)")
        choix = int(input("Abonne : 1 | Guichet : 2 | Automate : 3 "))
        if choix == 1:
            typeFournisseurPlace = "abonne"
        elif choix == 2:
            typeFournisseurPlace = "guichet"
        elif choix == 3:
            typeFournisseurPlace = "automate"

        choix = int(input("Camion : 1 | Deux roues : 2 | Vehicule simple : 3 "))
        if choix == 1:
            typeVehicule = "camion"
        elif choix == 2:
            typeVehicule = "2 roues"
        elif choix == 3:
            typeVehicule = "vehicule simple"
        nomParking = input("Quel est le nom du parking concerne par la réservation?")
        fonction.reservationOccasionnel(
            conn, date, typeFournisseurPlace, typeVehicule, nomParking
        )

    elif user_choice == 18:
        fonction.majReduction(conn)

    # SOUS MENU 2

    elif user_choice == 21:
        nomZone = input("Quel est le nom de la nouvelle zone ?")
        prixZone = float(input("Quel est le prix associé à la zone ?"))
        fonction.ajouterZone(conn, nomZone, prixZone)

    elif user_choice == 22:
        fonction.ajouterParking(conn)

    elif user_choice == 23:
        nomZone = input("nom de la zone à modifier :")
        nouveauTarif = input("Quel est le nouveau tarif de la zone ?")
        fonction.modifierTarifZone(conn, nomZone, nouveauTarif)

    elif user_choice == 24:
        fonction.nbPlacesDansParking(conn)

    elif user_choice == 25:
        parking = input("nom du parking à modifier :")
        zone = input("zone à attribuer au parking :")
        fonction.modifierZoneParking(conn, parking, zone)

    # SOUS MENU 3

    elif user_choice == 31:
        fonction.nombrePlacesPrises(conn)

    elif user_choice == 32:
        fonction.NbreTicketsParkingsParticulier(conn)

    elif user_choice == 33:
        fonction.NbreTicketsParkings(conn)

    elif user_choice == 34:
        fonction.nombreAbonne(conn)

    elif user_choice == 35:
        fonction.nombreOccasionnel(conn)

    elif user_choice == 36:
        fonction.nombreUtilisateurs(conn)

    elif user_choice == 37:
        fonction.nombreDeuxRouesEnregistres(conn)

    elif user_choice == 38:
        fonction.nombreCamionsEnregistres(conn)

    elif user_choice == 39:
        fonction.nombreVSEnregistres(conn)

    # SOUS MENU 4

    if user_choice == 41:
        data: str = ""
        with open("Insert.sql", "r") as f:
            for l in f:
                data += l
        cur.execute(data)
        conn.commit()
        print("donnees inserees \n\n")

    elif user_choice == 42:
        # create bdd
        bdd: str = ""
        with open("CREATE.sql", "r") as f:
            for l in f:
                bdd += l
        cur.execute(bdd)
        conn.commit()

    elif user_choice == 43:
        fonction.supprimerBDD(conn)
