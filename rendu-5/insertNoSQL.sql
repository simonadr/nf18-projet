INSERT INTO utilisateur VALUES ( 1 );
INSERT INTO utilisateur VALUES ( 2 );
INSERT INTO utilisateur VALUES ( 3 );
INSERT INTO utilisateur VALUES ( 4 );
INSERT INTO utilisateur VALUES ( 5 );
INSERT INTO utilisateur VALUES ( 6 );
INSERT INTO utilisateur VALUES ( 7 );
INSERT INTO utilisateur VALUES ( 8 );
INSERT INTO utilisateur VALUES ( 9 );
INSERT INTO utilisateur VALUES ( 10 );

INSERT INTO vehicule VALUES ('FH-338-RC', '{"modele":"Modus","marque":"Renault","typeVehicule":"vehicule simple"}', 1);
INSERT INTO vehicule VALUES ('FH-440-RC', '{"modele":"Modus","marque":"Renault","typeVehicule":"vehicule simple"}',2);
INSERT INTO vehicule VALUES ('667-LDO-345', '{"modele":"Alpine","marque":"Renault","typeVehicule":"vehicule simple"}', 3);
INSERT INTO vehicule VALUES ('28-XXX-TT', '{"modele":"458-Italia","marque":"Ferrari","typeVehicule":"vehicule simple"}', 4);
INSERT INTO vehicule VALUES ('ZD-111-RC', '{"modele":"Chiron","marque":"Bugati","typeVehicule":"vehicule simple"}', 5);
INSERT INTO vehicule VALUES ('AAA-001-AAA', '{"modele":"1299-Pagani Edition Finale","marque":"Ducati","typeVehicule":"2 roues"}', 6);
INSERT INTO vehicule VALUES ('ER-338-RC', '{"modele":"Polo","marque":"Wolkswagen","typeVehicule":"vehicule simple"}', 7);
INSERT INTO vehicule VALUES ('FH-440-RB', '{"modele":"Clio 3 RS","marque":"Renault","typeVehicule":"vehicule simple"}', 8);
INSERT INTO vehicule VALUES ('245-PTR-123','{"modele":"Model S","marque":"Tesla","typeVehicule":"vehicule simple"}', 9);
INSERT INTO vehicule VALUES ('123-ASW-549', '{"modele":"train","marque":"tchoutchou","typeVehicule":"vehicule simple"}',10);

INSERT INTO zone VALUES ('Portuaire', 15);
INSERT INTO zone VALUES ('Parc', 5);
INSERT INTO zone VALUES ('Ville', 10);
INSERT INTO zone VALUES ('ZII', 8);
INSERT INTO zone VALUES ('ZAC', 8);
INSERT INTO zone VALUES ('ZUC', 8);
INSERT INTO zone VALUES ('ZEP', 8);
INSERT INTO zone VALUES ('Paris 16', 100);
INSERT INTO zone VALUES ('Campagne', 1);
INSERT INTO zone VALUES ('Aeroport', 15);


INSERT INTO parking VALUES ('Bouvines', 'Ville','{"NbreDeuxRouesCouvertes":"53","NbreDeuxRouesAir":"72","NbreCamionCouvertes":"66","NbreCamionAir":"89","NbreSimpleCouvertes":"91","NbreSimpleAir":"46"}');
INSERT INTO parking VALUES ('Songeon', 'Parc','{"NbreDeuxRouesCouvertes":"23","NbreDeuxRouesAir":"92","NbreCamionCouvertes":"87","NbreCamionAir":"100","NbreSimpleCouvertes":"11","NbreSimpleAir":"6"}');
INSERT INTO parking VALUES ('Marche aux Herbes', 'Ville','{"NbreDeuxRouesCouvertes":"13","NbreDeuxRouesAir":"96","NbreCamionCouvertes":"17","NbreCamionAir":"12","NbreSimpleCouvertes":"21","NbreSimpleAir":"99"}');
INSERT INTO parking VALUES ('Bayser', 'Parc','{"NbreDeuxRouesCouvertes":"122","NbreDeuxRouesAir":"196","NbreCamionCouvertes":"171","NbreCamionAir":"172","NbreSimpleCouvertes":"201","NbreSimpleAir":"999"}');
INSERT INTO parking VALUES ('Chateau', 'Parc','{"NbreDeuxRouesCouvertes":"321","NbreDeuxRouesAir":"122","NbreCamionCouvertes":"343","NbreCamionAir":"92","NbreSimpleCouvertes":"28","NbreSimpleAir":"122"}');
INSERT INTO parking VALUES ('Gare', 'Ville','{"NbreDeuxRouesCouvertes":"111","NbreDeuxRouesAir":"232","NbreCamionCouvertes":"53","NbreCamionAir":"93","NbreSimpleCouvertes":"28","NbreSimpleAir":"231"}');
INSERT INTO parking VALUES ('La Ruche', 'ZAC','{"NbreDeuxRouesCouvertes":"13","NbreDeuxRouesAir":"22","NbreCamionCouvertes":"32","NbreCamionAir":"29","NbreSimpleCouvertes":"82","NbreSimpleAir":"12"}');
INSERT INTO parking VALUES ('Port', 'ZUC','{"NbreDeuxRouesCouvertes":"89","NbreDeuxRouesAir":"12","NbreCamionCouvertes":"43","NbreCamionAir":"92","NbreSimpleCouvertes":"38","NbreSimpleAir":"12"}');
INSERT INTO parking VALUES ('BF', 'ZII','{"NbreDeuxRouesCouvertes":"42","NbreDeuxRouesAir":"32","NbreCamionCouvertes":"38","NbreCamionAir":"119","NbreSimpleCouvertes":"238","NbreSimpleAir":"111"}');
INSERT INTO parking VALUES ('PG', 'ZEP','{"NbreDeuxRouesCouvertes":"1","NbreDeuxRouesAir":"2","NbreCamionCouvertes":"3","NbreCamionAir":"9","NbreSimpleCouvertes":"8","NbreSimpleAir":"1"}');

INSERT INTO compte VALUES ('louislech2001@gmail.com','Louis2001', False);
INSERT INTO compte VALUES ('pierre.guerin0@gmail.com','PIERRE00', True);
INSERT INTO compte VALUES ('adrisim2001@gmail.com','ADRI2001', True);
INSERT INTO compte VALUES ('samuel@gmail.com','Samuel64', False);
INSERT INTO compte VALUES ('Jacquie@gmail.com','jacquie666', True);
INSERT INTO compte VALUES ('OUIOUI@hotmail.com','ouiiii', True);
INSERT INTO compte VALUES ('rubynikara@wanadoo.fr','ruby98', False);
INSERT INTO compte VALUES ('lionel@beauxis.com','GRANDISS', False);
INSERT INTO compte VALUES ('pierrot@gmail.com','1234', True);
INSERT INTO compte VALUES ('antoine@dupont.com','Ministre', False);

INSERT INTO occasionnel VALUES (1,'louislech2001@gmail.com');
INSERT INTO occasionnel VALUES (2,'samuel@gmail.com');
INSERT INTO occasionnel VALUES (3,'lionel@beauxis.com');
INSERT INTO occasionnel VALUES (4,'rubynikara@wanadoo.fr');
INSERT INTO occasionnel VALUES (5,'antoine@dupont.com');

INSERT INTO ticket VALUES (1, '2021-05-08 15:30', 'guichet',1, 'Bouvines');
INSERT INTO ticket VALUES (2, '2021-05-09 11:30', 'guichet', 2,'Bouvines');
INSERT INTO ticket VALUES (3, '2021-05-09 12:35', 'guichet', 3,'Bouvines');
INSERT INTO ticket VALUES (4, '2021-05-10 12:17', 'automate',4, 'Songeon');
INSERT INTO ticket VALUES (5, '2021-05-13 12:30', 'automate', 5,'Songeon');
INSERT INTO ticket VALUES (6, '2021-05-14 08:35', 'automate', 1,'BF');
INSERT INTO ticket VALUES (7, '2021-05-14 10:15', 'automate', 2,'PG');
INSERT INTO ticket VALUES (8, '2021-05-14 18:30', 'automate', 3,'Port');

INSERT INTO personne VALUES ('pierre.guerin0@gmail.com', 'Guerin', 'Pierre');
INSERT INTO personne VALUES ('adrisim2001@gmail.com', 'Simon', 'Adrien');
INSERT INTO personne VALUES ('Jacquie@gmail.com', 'Jacques', 'Jacquie');
INSERT INTO personne VALUES ('OUIOUI@hotmail.com', 'Oui', 'Oui');
INSERT INTO personne VALUES ('pierrot@gmail.com', 'roppie', 'pierrot');


INSERT INTO abonne VALUES (1,1, 'pierre.guerin0@gmail.com');
INSERT INTO abonne VALUES (2,2, 'adrisim2001@gmail.com');
INSERT INTO abonne VALUES (3,10, 'Jacquie@gmail.com');
INSERT INTO abonne VALUES (4,8, 'OUIOUI@hotmail.com');
INSERT INTO abonne VALUES (5, 2, 'pierrot@gmail.com');

INSERT INTO abonnement VALUES (1,'{"prix":"150","duree":"30","dateFin":"01-06-2022"}','Bouvines',2);
INSERT INTO abonnement VALUES (2, '{"prix":"100","duree":"50","dateFin":"02-07-2022"}','Songeon',3);
INSERT INTO abonnement VALUES (3,'{"prix":"50","duree":"90","dateFin":"01-08-2022"}','Bouvines',4);
INSERT INTO abonnement VALUES (4, '{"prix":"75","duree":"45","dateFin":"01-03-2022"}'
,'BF',5);
INSERT INTO abonnement VALUES (5,'{"prix":"150","duree":"45","dateFin":"02-05-2022"}'
,'PG',1);
INSERT INTO abonnement VALUES (6, '{"prix":"100","duree":"30","dateFin":"15-06-2022"}'
,'Port',1);
INSERT INTO abonnement VALUES (7,'{"prix":"70","duree":"75","dateFin":"31-04-2022"}'
,'Songeon',3);


INSERT INTO place VALUES (1,'camion','Bouvines');
INSERT INTO place VALUES (2,'vehicule simple', 'Bouvines');
INSERT INTO place VALUES (3,'vehicule simple','BF');
INSERT INTO place VALUES (4,'2 roues','BF');
INSERT INTO place VALUES (5,'2 roues', 'PG');
INSERT INTO place VALUES (6,'vehicule simple','Gare');
INSERT INTO place VALUES (7,'camion','Gare');
INSERT INTO place VALUES (8,'vehicule simple', 'Bayser');
INSERT INTO place VALUES (9,'vehicule simple','Chateau');
INSERT INTO place VALUES (10,'2 roues','La Ruche');

INSERT INTO reservation VALUES (1, '2021-05-09 11:30', 'abonne', 'pierre.guerin0@gmail.com',2,'Bouvines');
INSERT INTO reservation VALUES (2, '2022-08-16 18:30', 'abonne', 'adrisim2001@gmail.com',3,'BF');
INSERT INTO reservation VALUES (3, '2022-08-17 18:30', 'automate', 'lionel@beauxis.com',7,'Gare');
INSERT INTO reservation VALUES (4, '2022-09-02 12:00', 'guichet', 'antoine@dupont.com',4,'BF');



